# giteePages

#### 介绍
最新静态页面

#### 软件说明
使用hexo提供技术支持
https://hexo.io/zh-cn/

需要**NodeJS** v12版本以上


#### 安装教程

1.  npm install hexo-cli
2.  npx hexo init blog
3.  cd blog
4.  npm install
5.  npx hexo server
6.  访问 [http://localhost:4000/](http://localhost:4000/ "")查看效果
 





#### 使用说明

1.  进入blog目录 npx hexo generate
2.  生成的文件会进入public
3.  注意_config.yml配置 
    例如
```
  url: https://****/gitee-pages 
  
  root: /gitee-pages/ #/
```


#### 主题
> https://github.com/JoeyBling/hexo-theme-yilia-plus

> https://github.com/Fechin/hexo-theme-diaspora

> https://github.com/ZEROKISEKI/hexo-theme-gal

> https://github.com/jinyaoMa/hexo-theme-mustom


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 赞助


