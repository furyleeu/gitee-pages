# giteePages

#### Description
Latest static page

#### Software Architecture
Powered by Hexo
You Should Read more at :  https://hexo.io/zh-cn/

Requires **NodeJS** v12 or above


#### Installation

1. npm install hexo-cli
2. npx hexo init blog
3. cd blog
4. npm install
5. npx hexo server
6. visit [http://localhost:4000/](http://localhost:4000/ "") demo

#### Instructions

1. Enter the blog directory **npx hexo generate**
2. The generated file will enter public
3. Pay attention to the _config.yml configuration
    E.g
```
  url: https://****/gitee-pages
  
  root: /gitee-pages/ #/
```


#### Themes
> https://github.com/JoeyBling/hexo-theme-yilia-plus

> https://github.com/Fechin/hexo-theme-diaspora

> https://github.com/ZEROKISEKI/hexo-theme-gal

> https://github.com/jinyaoMa/hexo-theme-mustom


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### Sponsor
